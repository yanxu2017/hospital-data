cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently
set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	preclean_outpatient_final
	/*把门诊final部分切成了4部分 使用prescripID进行match
	1. outpatient_prescription2012-2019.dta 所有和处方信息相关
	2. outpatient_prescription_other2012-2019.dta 原始数据中和demographic相关的信息
	3. outpatient_diagnosis2012-2019.dta 诊断信息，病情描述，病人相关病史。文字信息较多
	4. prescriptionid_dictionary2012-2019.dta 所有原始数据中的id信息, prescriptionid,innerid,patientid
	每年的数据中，drop错行问题严重的，并把drop过的obs存储于 outpatient_drop2012-2019.dta
	*/
	preclean_cardinfo
	/*please update the cardinfo.csv under external\门诊数据3 */
	preclean_outpatient_data3
	/*please update the csv file in external\门诊数据3_more */
	gen_innerid_key
	/*generate a cleanup unique innerid list from prescription data */
	*fill_cardinfo
	/*there are some demographic info in the prescription data, append this part to the cardinfo */
	fill_cardinfo
	/*complete the cardinfo by the demo info from outpatient prescription*/
	clean_department
	preclean_unique_patientkey
	/*as the inneridkey cannot uniquely identify one patient with many doctor visits, here we use name birthday to uniquely identify one patient. Assign 
	uniqueid to identify one patient*/
	preclean_diagnosis
	/*clean up the diagnosis information*/
	preclean_diagnosis_severity
	/*clean up the diagnosis information with severity tagged only for the department of dermatology*/
end


program preclean_outpatient_final
	forvalue k = 2012(1)2019 {
		import delimited "..\external\门诊数据final\门诊数据`k'.csv", encoding(UTF-8) clear
		g prescripID = _n
		drop v23 
		g dataSetYr = `k'
		rename (v1 v2 v5 v6 v7 v8 v10 v14 v15 v18 v20 v21 v22 v13 v3) ///
		(prescriptionid patientid birthday ///
		age doctorid2 itemcode amount ///
		diagnosisid time ///
		cost innerid ///
		mainsymptoms medicalhistory diagnosis ///
		name)
		if `k' == 2014 {
			replace v12 = doctorid2
		}
		drop doctorid2
		split birthday,p("-")
		drop birthday
		rename (birthday1 birthday2 birthday3) (birthYr birthMon birthDay)
		destring birth*, replace force
		
		* check missing, drop observations if nomiss < 20
		egen nonmiss = rownonmiss(_all), strok
		preserve
			*???? why you use 20 as the threshold????
		* there are total 23 variables, if there are less than 20 variables, the observation is abnormal
			keep if nonmiss < 20
			keep prescripID prescriptionid patientid dataSetYr
			if `k' == 2012 save ../temp/outpatient_drop.dta, replace
			if `k' > 2012 {
				append using ../temp/outpatient_drop.dta
				save ../temp/outpatient_drop.dta, replace
			} 
			
		restore
		drop if nonmiss < 20
		drop nonmiss
		
		*cleanup time, drop observatoins if yrlen ~= 4
		replace time = "" if time=="NULL"
		split(time), p(" ")
		split(time1), p("-")
		rename (time11 time12 time13) (prescriptionYr prescriptionMo prescriptionDay)
		split time2, p(":")
		rename (time21 time22) (prescriptionHr prescriptionMinute)
		g yrlen = strlen(prescriptionYr)
		preserve
			keep if yrlen != 4
			keep prescripID prescriptionid patientid dataSetYr 
			append using ../temp/outpatient_drop.dta
			save ../temp/outpatient_drop.dta, replace
		restore
		drop if yrlen != 4
		drop yrlen
		destring prescriptionYr prescriptionMo prescriptionDay prescriptionHr prescriptionMinute, replace
		drop time time1 time2 time23

		*cleanup gender, drop observations if gender variable doesn't make sense
		preserve 
			keep if !inlist(v4, "男","女","NULL")
			keep prescripID prescriptionid patientid dataSetYr
			append using ../temp/outpatient_drop.dta
			save ../temp/outpatient_drop.dta, replace
		restore		
		drop if !inlist(v4, "男","女","NULL")
		
		*cleanup amount cost, drop observations if value of amount variable doesn't make sense
		replace amount = "." if amount == "NULL"
		replace amount = "." if amount == ".."
		g amountend = substr(amount,-1,1)
		g amountstart = substr(amount,1,1)
		replace amount = subinstr(amount,".","",.) if amountend =="."
		replace amount = "0"+amount if amountstart == "."		
		replace amount = subinstr(amount,"","0",.)
		drop amountend amountstart
		g amountcheck = substr(amount,1,1)
		replace amountcheck = "1" if amountcheck=="6" | amountcheck=="7" | amountcheck=="8" | amountcheck=="9"
		preserve
			keep if !inlist(amountcheck,".","0","1","2","3","4","5")
			keep prescripID prescriptionid patientid dataSetYr
			append using ../temp/outpatient_drop.dta
			save ../temp/outpatient_drop.dta, replace
		restore
		drop if !inlist(amountcheck,".","0","1","2","3","4","5")
		destring amount, replace
		drop amountcheck
		
		*cleanup cost, drop observations if the value of cost variable doesn't make sense
		replace cost = "." if cost == "NULL" | cost ==""
		replace cost = subinstr(cost," ","",.)
		g costcheck = substr(cost,1,1)
		replace costcheck = "1" if costcheck == "6" | costcheck == "7" | costcheck == "8" | costcheck == "9"
		preserve
		keep if !inlist(costcheck,".","0","1","2","3","4","5")
			keep prescripID prescriptionid patientid dataSetYr
			append using ../temp/outpatient_drop.dta	
			save ../temp/outpatient_drop.dta, replace
		restore
		drop if !inlist(costcheck,".","0","1","2","3","4","5")		
		destring cost, replace
		drop costcheck
		
		
		preserve
			duplicates drop
			keep prescripID prescriptionid patientid innerid dataSetYr 						
			save ../temp/prescriptionid_dictionary`k'.dta, replace
		restore
		preserve
			drop prescriptionid patientid name birthYr birthMon birthDay age diagnosisid diagnosis mainsymptoms medicalhistory innerid v4 v2*
			*only the data of 2019 and 2018 have anomalous variables with name v3*
			if `k' == 2019 | `k' == 2018 drop v3*
			duplicates drop
			save ../temp/outpatient_prescription`k', replace
		restore
		preserve
			*I added v16(department) here v19(status) v17(departmentid) here as I'm not sure whether they also relate to diagnosis...
			*?? if v16 v17 v19 are encoded in different data files, will conflicts of the encode number happen
			keep prescripID patientid diagnosisid diagnosis mainsymptoms medicalhistory dataSetYr v16 v19 v17 
			duplicates drop
			save ../output/outpatient_diagnosis`k'.dta, replace
		restore
		preserve
			keep prescripID patientid name v4 birthYr birthMon birthDay age dataSetYr 
			duplicates drop
			save ../temp/outpatient_prescription_other`k'.dta, replace
		restore
		display "year `k'"		
	}
	
	*cleanup the outpatient_final_more data
	forvalues k=2012(1)2019{
		import delimited "..\..\derived\external\门诊数据final_more\门诊数据补充`k'.csv", encoding(UTF-8) clear
		drop if v1 =="test"
		ta v3 v4
		replace v4 = v3 if v4=="NULL" & v3 !="NULL"
		replace v4 = "西药" if v4 =="1"
		replace v4 = "成药" if v4 =="2"
		replace v4 = "中草药" if v4 =="3"
		replace v4 = "检查" if v4 =="4"
		replace v4 = "化验" if v4 =="5"
		replace v4 = "治疗" if v4 =="7"
		replace v4 = "高值耗材" if v4 =="8"
		replace v4 = "PET-CT" if v4 =="401"
		*the type info in v3 has been merged into v4, so v3 is dropped
		drop v3
		rename v4 type
		rename v2 doctorid_final_more
		rename v1 prescriptionid
		*double check there is no duplicates. no obs is dropped
		duplicates drop prescriptionid doctorid type, force
		mmerge prescriptionid using ../temp/prescriptionid_dictionary`k', type(1:1) unmatch(master) ukeep(prescripID)
		drop prescriptionid _merge
		save ../temp/outpatient_final_more`k', replace
		display "year=`k'"
	}
	
	*complement type and doctorid to outpatient_prescription, append outpatient_prescription`k' from each year into one file
	forvalues k = 2012(1)2019{	
		use ../temp/outpatient_final_more`k',clear
		sort prescripID		
		mmerge prescripID using ../temp/outpatient_prescription`k', type(n:1) unmatch(using)
		drop _merge
		save ../temp/outpatient_prescription`k', replace
		if `k' == 2012 save ../temp/outpatient_prescription, replace
		if `k' >2012 {
			append using ../temp/outpatient_prescription
			save ../temp/outpatient_prescription, replace
		}
		dis "year=`k'"
	}
	*combine the prescription_other files into one
	forvalues k = 2012(1)2019{
		use ../temp/outpatient_prescription_other`k', clear
		if `k' > 2012 append using ../temp/outpatient_prescription_other
		save ../temp/outpatient_prescription_other, replace
	}
	
	*encode the variables in prescription file
	use ../temp/outpatient_prescription.dta, clear 
	encode v9, g(itemname)
	encode v11, g(unit)
	rename v12 doctorname
	encode v19, g(status)
	encode type, g(ntype)
	rename (v16 v17) (department departmentid)
	drop v9 v11 v19	type 
	rename (ntype) (type)
	save ../output/outpatient_prescription.dta, replace
	
	*currently there is no need to append each diagnosis file into one file
// 	use ../temp/outpatient_diagnosis.dta, clear
// 	encode v16, g(department)
// 	encode v17, g(departmentid)
// 	encode v19, g(status)	
// 	drop v*
// 	save ../temp/outpatient_diagnosis.dta, replace
	
	use ../temp/outpatient_prescription_other.dta, clear
	encode(v4), g(gender)
	drop v4
	save ../temp/outpatient_prescription_other.dta, replace
	*let's evaluate this in sample selection part
	/*
	forvalues k =2012(1)2019{
		use ../temp/outpatient_prescription`k'.dta, clear
		* ???? why drop those observations here???? we will perform sample selection later.
		preserve
			keep if amount > 100000 | cost > 100000
			append using ../temp/outpatient_drop`k'
			save ../temp/outpatient_drop`k', replace
		restore
		drop if amount > 100000 | cost >100000
		save ../temp/outpatient_prescription`k', replace
	}
	*/
end


program preclean_cardinfo
	import delimited "..\external\门诊数据3\cardinfo.csv", encoding(UTF-8) clear
	rename (v1 v2 v3 v5 v6 v7) ///
	(innerid patientid name idcard address phone)
	encode v8, g(gender)
	drop v8
	split v4, p(" ")
	drop v42 v4
	split v41, p("-")
	rename (v411 v412 v413) (birthYr birthMon birthDay)
	replace birthYr="." if birthYr=="NULL"
	destring(birth*), replace
	drop v41
	split v9, p(" ")
	split v91, p("-")
	split v92, p(":")
	rename (v911 v912 v913) (cardsetupYr cardsetupMo cardsetupDay)
	rename (v921 v922) (cardsetupHr cardsetupMin)
	drop v9 v923 v91 v92
	destring (cardsetup*), replace
	su
	replace birthYr=. if birthYr>2019 | birthYr<1900
	* drop if innerid is missing, 1 observation is dropped
	drop if missing(innerid)
	*clean up phone 
	g index = _n
	replace phone = subinstr(phone," ","",.)
	replace phone = subinstr(phone,"\","",.)
	replace phone = subinstr(phone,"]","",.)
	replace phone = subinstr(phone,"*","",.)
	replace phone = subinstr(phone,"‘","",.)
	replace phone = subinstr(phone,"、","",.)
	replace phone = subinstr(phone,"=","",.)
	replace phone = subinstr(phone,"】","",.)
	replace phone = subinstr(phone,"１","1",.)
	replace phone = subinstr(phone,"２","2",.)
	replace phone = subinstr(phone,"３","3",.)
	replace phone = subinstr(phone,"４","4",.)
	replace phone = subinstr(phone,"５","5",.)
	replace phone = subinstr(phone,"６","6",.)
	replace phone = subinstr(phone,"７","7",.)
	replace phone = subinstr(phone,"８","8",.)
	replace phone = subinstr(phone,"９","9",.)
	replace phone = subinstr(phone,"０","0",.)
	replace phone = subinstr(phone,":","",.)
	replace phone = "." if phone == "NULL"
	g phonecheck = 1 if regexm(phone,"[^0-9.]")
	* drop if "phone" != 1, 4481 observations are dropped
	preserve
		keep if phonecheck ==1
		keep index
		save ../temp/cardinfo_drop, replace
	restore
	keep if phonecheck != 1
	drop phonecheck
	* clean up idcard 
	replace idcard = subinstr(idcard," ","",.)
	replace idcard = subinstr(idcard,"[","",.)
	replace idcard = subinstr(idcard,"]","",.)
	replace idcard = subinstr(idcard,"、","",.)
	replace idcard = subinstr(idcard,"‘","",.)
	replace idcard = subinstr(idcard,"‘’","",.)
	replace idcard = subinstr(idcard,"\","",.)
	replace idcard = subinstr(idcard,"=","",.)
	replace idcard = subinstr(idcard,"】","",.)
	replace idcard = subinstr(idcard,"-","",.)
	replace idcard = subinstr(idcard,"*","",.)
	replace idcard = subinstr(idcard,"１","1",.)
	replace idcard = subinstr(idcard,"２","2",.)
	replace idcard = subinstr(idcard,"３","3",.)
	replace idcard = subinstr(idcard,"４","4",.)
	replace idcard = subinstr(idcard,"５","5",.)
	replace idcard = subinstr(idcard,"６","6",.)
	replace idcard = subinstr(idcard,"７","7",.)
	replace idcard = subinstr(idcard,"８","8",.)
	replace idcard = subinstr(idcard,"９","9",.)
	replace idcard = subinstr(idcard,"０","0",.)
	replace idcard = subinstr(idcard,":","",.)	
	replace idcard = "" if idcard =="无" | idcard =="暂缺" | idcard =="未带" | idcard =="未携带" | idcard =="未提供" | idcard == "未知" | idcard =="不详" | idcard == "没有"	
	*clean up innerid
	replace innerid = subinstr(innerid,"１","1",.)
	replace innerid = subinstr(innerid,"２","2",.)
	replace innerid = subinstr(innerid,"３","3",.)
	replace innerid = subinstr(innerid,"４","4",.)
	replace innerid = subinstr(innerid,"５","5",.)
	replace innerid = subinstr(innerid,"６","6",.)
	replace innerid = subinstr(innerid,"７","7",.)
	replace innerid = subinstr(innerid,"８","8",.)
	replace innerid = subinstr(innerid,"９","9",.)
	replace innerid = subinstr(innerid,"０","0",.)
	replace innerid = subinstr(innerid,"/","",.)	
	replace innerid = subinstr(innerid," ","",.)
	save ../temp/cardinfo, replace
end


program preclean_outpatient_data3
	import delimited "..\..\derived\external\门诊数据3_more\2013-2015.csv", varnames(1) encoding(UTF-8) clear 
	rename someinnerid registrationid
	save ../temp/outpatient_data3more.dta, replace
	import delimited "..\..\derived\external\门诊数据3_more\2016-2019.csv", varnames(1) encoding(UTF-8) clear 
	rename someinnerid registrationid
	append using ../temp/outpatient_data3more.dta, force	
	save ../temp/outpatient_data3more.dta, replace
	
	use ../temp/outpatient_data3more.dta, clear
	split date2, p(" ")
	split date21, p("-")
	split date22, p(":")
	rename (date211 date212 date213) (registrationYr registrationMo registrationDay)
	rename (date221 date222 date223) (registrationHr registrationMin registrationSecond)
	rename 身份证id idcard
	drop date*
	destring registration*, replace
	replace gender = "." if gender == "NULL"
	destring gender, replace
	encode (paymenttype), g(payment)
	drop paymenttype
	drop if registrationid==""
	save ../temp/outpatient_data3more.dta, replace

	import delimited "..\..\derived\external\门诊数据3\2013-2015.csv", varnames(1) encoding(UTF-8) clear
	save ../temp/outpatient_data3.dta, replace
	import delimited "..\..\derived\external\门诊数据3\2016-2019.csv", varnames(1) encoding(UTF-8) clear
	append using ../temp/outpatient_data3.dta, force
	rename (doctorid categoryid) (doctoriddrop categoryiddrop)
	encode doctoriddrop, g(doctorid)
	encode categoryiddrop, g(categoryid)
	drop doctoriddrop categoryiddrop
	drop registrationtime
	save ../temp/outpatient_data3.dta, replace
	
	mmerge registrationid using ../temp/outpatient_data3more, type(1:1) unmatched(both)
	keep if _merge==3
	*drop the unmatched obs which account for only 1.18%
	drop _merge
	save ../temp/outpatient_registration.dta, replace
	
	use ../temp/outpatient_registration, clear
	replace innerid = subinstr(innerid,"１","1",.)
	replace innerid = subinstr(innerid,"２","2",.)
	replace innerid = subinstr(innerid,"３","3",.)
	replace innerid = subinstr(innerid,"４","4",.)
	replace innerid = subinstr(innerid,"５","5",.)
	replace innerid = subinstr(innerid,"６","6",.)
	replace innerid = subinstr(innerid,"７","7",.)
	replace innerid = subinstr(innerid,"８","8",.)
	replace innerid = subinstr(innerid,"９","9",.)
	replace innerid = subinstr(innerid,"０","0",.)
	replace innerid = subinstr(innerid,"/","",.)	
	replace innerid = subinstr(innerid," ","",.)
	drop registrationid
	save ../output/outpatient_registration.dta, replace
end


program gen_innerid_key
	forvalues k = 2012(1)2019{
		use ../temp/prescriptionid_dictionary`k'.dta,clear
		replace innerid = subinstr(innerid,"１","1",.)
		replace innerid = subinstr(innerid,"２","2",.)
		replace innerid = subinstr(innerid,"３","3",.)
		replace innerid = subinstr(innerid,"４","4",.)
		replace innerid = subinstr(innerid,"５","5",.)
		replace innerid = subinstr(innerid,"６","6",.)
		replace innerid = subinstr(innerid,"７","7",.)
		replace innerid = subinstr(innerid,"８","8",.)
		replace innerid = subinstr(innerid,"９","9",.)
		replace innerid = subinstr(innerid,"０","0",.)
		replace innerid = subinstr(innerid,"/","",.)	
		replace innerid = subinstr(innerid," ","",.)
		save ../temp/prescriptionid_dictionary`k'.dta, replace
		duplicates drop innerid, force
		keep innerid dataSetYr
		if `k' > 2012 append using ../temp/prescription_innerid_list.dta
		save ../temp/prescription_innerid_list.dta, replace		
		dis "`k'"
	}
	
	use ../temp/prescription_innerid_list.dta, clear
	duplicates drop innerid, force
	save ../temp/prescription_innerid_list.dta, replace
	
	*generate a cleanup unique innerid list from registration data
	use ../output/outpatient_registration.dta, clear
	keep innerid
	duplicates drop innerid, force
	save ../temp/registration_innerid_list.dta, replace
	
	*combine the innerids from registration, prescription and cardinfo
	use ../temp/cardinfo,clear
	keep innerid
	append using ../temp/registration_innerid_list.dta 
	append using ../temp/prescription_innerid_list.dta, 
	duplicates drop innerid, force
	g inneridkey = _n
	save ../temp/innerid_dictionary.dta,replace
	
	*replace all the innerid by inneridkey
	forvalues k = 2012(1)2019{
		use ../temp/prescriptionid_dictionary`k'.dta, clear
		mmerge innerid using ../temp/innerid_dictionary.dta, type(n:n) unmatched(master)
		drop innerid _merge
		dis "`k'"
		save ../temp/prescriptionid_dictionary`k'.dta,replace
	}
	use ../output/outpatient_registration.dta, clear
	mmerge innerid using ../temp/innerid_dictionary.dta, type(n:n) unmatched(master) 
	drop innerid _merge
	save ../output/outpatient_registration.dta, replace
	use ../temp/cardinfo, clear
	mmerge innerid using ../temp/innerid_dictionary.dta, type(n:n) unmatched(master)
	drop innerid _merge
	save ../temp/cardinfo.dta, replace

	forvalues k = 2012(1)2019{
		use ../temp/prescriptionid_dictionary`k'.dta, clear
		if `k' > 2012 append using ../temp/prescriptionid_dictionary.dta
		save ../temp/prescriptionid_dictionary.dta, replace		
		dis "`k'"
	}
	save ../output/prescriptionid_dictionary.dta, replace
end


program fill_cardinfo
	use ../temp/outpatient_prescription_other.dta, clear
	mmerge prescripID dataSetYr using ../temp/prescriptionid_dictionary.dta, type(1:1) ukeep(inneridkey) unmatch(master)
	drop _merge prescripID
	duplicates drop inneridkey, force
	save ../temp/outpatient_prescription_other.dta, replace
	
	use ../temp/cardinfo.dta, clear
	mmerge inneridkey using ../temp/outpatient_prescription_other.dta, type(1:1) unmatched(both) update
	drop age _merge patientid
	save ../output/cardinfo_rev.dta, replace
end


program clean_department
	use ../output/outpatient_prescription, clear
	rename department departmentold
	g department = departmentold
	* cleanup the department name based on 学科发展最终版本.xls
	renamefunciton
	save ../output/outpatient_prescription, replace 
end


program preclean_unique_patientkey
	*as the inneridkey cannot uniquely identify one patient with many doctor visits, here we use name birthday to uniquely identify one patient. Assign 
	*uniqueid to identify one patient
	use ../output/cardinfo_rev, clear
	sort birthYr birthMon birthDay name gender
	bys birthYr birthMon birthDay name gender: g count = _N
	tab count
	*checked the highly repetitive patientid (count>`r(p99)') and found three cases that is due to mistake in the system
	drop if (name == "1" | name == "yyy" | name == "王") & count > 60
	replace idcard = "" if idcard == "NULL"
	replace address = "" if address == "NULL"
	replace phone = "" if phone == "NULL"
	egen unmiss = rownonmiss(_all), strok
	replace unmiss = - unmiss
	sort birthYr birthMon birthDay name unmiss
	bys birthYr birthMon birthDay name gender: g id = _n
	sort id
	g uniqueid_temp = _n if id == 1
	bys birthYr birthMon birthDay name gender: egen uniqueid = mean(uniqueid_temp)
	keep uniqueid inneridkey
	save ../temp/uniqueid_innerid.dta, replace
			
	use ../output/prescriptionid_dictionary, clear
	mmerge inneridkey using ../temp/uniqueid_innerid.dta, type(n:1) ukeep(uniqueid) unmatch(master)
	drop _merge
	save ../output/prescriptionid_dictionary.dta, replace
	
	use ../output/prescriptionid_dictionary, clear
	g len = strlen(patientid)
	g insuranceVisit = 1 if len == 19
	replace insuranceVisit = 0 if len == 8
	save ../output/prescriptionid_dictionary, replace

	use ../output/prescriptionid_dictionary, clear	
	keep if len == 8 | len == 19
	bys uniqueid: egen lenmean = mean(len)
	g insurance = 1 if lenmean != 8
	replace insurance = 0 if lenmean == 8
	keep uniqueid insurance
	duplicates drop uniqueid, force
	drop if missing(uniqueid)
	save ../output/insurancelist, replace
	
end

program preclean_diagnosis

	forvalues year = 2016(1)2019{
		di "`year'"
		use ../output/outpatient_diagnosis`year', clear
		rename v16 department
		renamefunciton
		keep if department == "皮肤性病科" | department == "耳鼻喉科、耳鼻喉肿瘤外科" | department == "口腔门诊" | department == "结直肠肛门外科、结直肠肿瘤外科" | department == "胸外科、胸部肿瘤外科"
		keep diagnosis diagnosisid department prescripID dataSetYr
		if `year' > 2016{
			append using ../temp/diagnosis
		}
		save ../temp/diagnosis, replace
	}
	
	use ../temp/diagnosis, clear
	replace diagnosis = subinstr(diagnosis,"?","",.)
	replace diagnosis = subinstr(diagnosis,"？","",.)
	replace diagnosis = subinstr(diagnosis," ","",.)
	replace diagnosis = subinstr(diagnosis,"..","",.)
	replace diagnosis = subinstr(diagnosis,"|","",.)
	g status = 0
	g firsttwoletter = substr(diagnosis, 1, 2)
	replace status = 1 if firsttwoletter == "11" | firsttwoletter == "37" | firsttwoletter == "16" | firsttwoletter == "1、" | firsttwoletter == "12"
	drop firsttwoletter
	split diagnosis, p("、")

	replace diagnosis1 = diagnosis if status == 1
	forvalues k = 2(1)6{
	
		replace diagnosis`k' = "" if status == 1
	}
	drop status

	replace diagnosis1 = diagnosis if diagnosis4 != ""
	drop diagnosis4 diagnosis5 diagnosis6
	replace diagnosis1 = diagnosis1+";" + diagnosis2 + ";" + diagnosis3
	replace diagnosis1 = subinstr(diagnosis1,";;","",.)
	drop diagnosis2 diagnosis3
	split diagnosis1, p(";")
	split diagnosisid, p(";")

	replace department = "dermatology" if department == "皮肤性病科"
	replace department = "ear" if department == "耳鼻喉科、耳鼻喉肿瘤外科"
	replace department = "tooth" if department == "口腔门诊"
	replace department = "rectum" if department == "结直肠肛门外科、结直肠肿瘤外科"
	replace department = "chest" if department == "胸外科、胸部肿瘤外科"
	drop diagnosis1
	save ../output/diagnosis_split, replace
	
	use ../output/diagnosis_split, clear
	forvalues k = 1(1)6{
		preserve
			keep diagnosis1`k' diagnosisid`k' department
			rename (diagnosis1`k' diagnosisid`k') (diagnosis diagnosisid)
			if `k' > 1{
				append using ../temp/diagnosislist
			}
			save ../temp/diagnosislist, replace
		restore
	}
	
	use ../temp/diagnosislist, clear
	bys department diagnosis: g n = _n
	keep if n == 1
	drop n
	save ../output/diagnosis_dictionary, replace
	
	use ../output/diagnosis_dictionary, clear
	sort department diagnosis
	export excel using "../output/diagnosis_dictionary.xlsx", firstrow(variables) replace
end

* before running the next function: preclean_diagnosis_severity, we need to update the diagnosis_dictionary_mannualCleanUp_tagged.xlsx, which contains the severity information tagged by the doctor.

program preclean_diagnosis_severity
	
	import excel using "../input/diagnosis_dictionary _manualCleanUp_tagged.xlsx", sheet("dermatology") firstrow clear
	save ../temp/diagnosis_dictionary_tagged, replace
		
	use ../output/diagnosis_split, clear
	keep if department == "dermatology"
	save ../temp/diagnosis_dermatology, replace
	
	forvalues k=1(1)6{
	
		use ../temp/diagnosis_dictionary_tagged, clear	
		rename (diagnosis severity) (diagnosis1`k' severity`k')
		save ../temp/diagnosis_dictionary_tagged1, replace
		
		use ../temp/diagnosis_dermatology, clear
		mmerge diagnosis1`k' using ../temp/diagnosis_dictionary_tagged1, type(n:1) ukeep(severity`k') unmatched(master)
		drop _merge
		g score`k' = 0
		replace score`k' = 1 if severity`k' == "low" | severity`k' == "moderate" | severity`k' == "high"
		save ../temp/diagnosis_dermatology, replace
		
	}
	
	use ../temp/diagnosis_dermatology, clear
	g noise = score1 + score2 + score3 + score4 + score5 + score6
	* noise == 0 means the diagnosis information is noise and needs to be ignored
	drop score*
	save ../output/diagnosis_dermatology, replace

end


program renamefunciton

	replace department = "内分泌糖尿病一科" if department == "内分泌一科"
	replace department = "内分泌糖尿病二科" if department == "内分泌二科"
	replace department = "内分泌糖尿病三科" if department == "内分泌三科"
	replace department = "呼吸与危重症医学科一科" if department == "呼吸内一科"
	replace department = "呼吸与危重症医学科二科" if department == "呼吸内二科"	
	replace department = "妇一科、妇科肿瘤外一科" if department == "妇一科"
	replace department = "妇二科、妇科肿瘤外二科" if department == "妇二科"	
	replace department = "心血管内一科" if department == "心血管一科"	
	replace department = "心血管内二科" if department == "心血管二科"
	replace department = "心血管内三科" if department == "心血管三科"
	replace department = "心血管内四科" if department == "心血管四科"
	replace department = "心血管内五科" if department == "心血管五科"
	replace department = "泌尿外一科、泌尿肿瘤外一科" if department == "泌尿外一科"
	replace department = "泌尿外二科、泌尿肿瘤外二科" if department == "泌尿外二科"
	replace department = "甲状腺乳腺外一科、甲状腺乳腺肿瘤外一科" if department == "甲状腺乳腺外一科"
	replace department = "甲状腺乳腺外二科、甲状腺乳腺肿瘤外二科" if department == "甲状腺乳腺外二科"
	replace department = "甲状腺乳腺外三科、甲状腺乳腺肿瘤外三科" if department == "甲状腺乳腺外三科"
	replace department = "甲状腺乳腺外四科、甲状腺乳腺肿瘤外四科" if department == "甲状腺乳腺外四科"
	replace department = "耳鼻喉科、耳鼻喉肿瘤外科" if department == "耳鼻喉科"
	replace department = "口腔颌面外科、口腔颌面肿瘤外科" if department == "口腔颌面外科"	
	replace department = "血液内一科" if department == "血液病内一科"
	replace department = "血液内二科" if department == "血液病内二科"	
	replace department = "内分泌糖尿病四科" if department == "糖尿病科"
	replace department = "甲状腺乳腺外一科、甲状腺乳腺肿瘤外一科" if department == "肿瘤外一科"	
	replace department = "甲状腺乳腺外二科、甲状腺乳腺肿瘤外二科" if department == "肿瘤外二科"
	replace department = "甲状腺乳腺外三科、甲状腺乳腺肿瘤外三科" if department == "肿瘤外三科"
	replace department = "甲状腺乳腺外四科、甲状腺乳腺肿瘤外四科" if department == "甲状腺外科"
	replace department = "甲状腺乳腺外一科、甲状腺乳腺肿瘤外一科" if department == "甲状腺乳腺外一科"	
	replace department = "甲状腺乳腺外二科、甲状腺乳腺肿瘤外二科" if department == "乳腺外科"
	replace department = "甲状腺乳腺外三科、甲状腺乳腺肿瘤外三科" if department == "甲状腺乳腺外三科"
	replace department = "甲状腺乳腺外四科、甲状腺乳腺肿瘤外四科" if department == "甲状腺乳腺外四科"	
	replace department = "结直肠肛门外科、结直肠肿瘤外科" if department == "肛肠外科"
	replace department = "肝胆胰外一科、肝胆胰肿瘤外一科" if department == "普通外一科"	
	replace department = "胃肠疝外科、胃肠肿瘤外二科" if department == "普通外二科"
	replace department = "肝胆胰外二科、肝胆胰肿瘤外二科" if department == "普通外三科"	
	replace department = "胸外科、胸部肿瘤外科" if department == "胸外科"

end


main
