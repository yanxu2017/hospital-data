**********************************************************
*
* Checkdta Report
*
* Create file, checkdta.log, that allows users to check
* whether Stata datasets have actually changed after
* make.bat is re-run.
*
**********************************************************

program define maxmem, rclass

	version 10
	local maxfsize 0
	
	local dirlist "..\temp\dirlog.txt"

	* read directory data from temporary file
	
	tempname fh
	
	file open `fh' using "`dirlist'", text read
	file read `fh' line
	
	local nfiles = 0

	while r(eof)==0  {

		if `"`line'"' ~= "" & substr(`"`line'"',1,1) ~= " " {

			if "`c(os)'" == "Windows" {
				local fsize : word 4 of `line'
				local fsize = subinstr("`fsize'",",","",.)
				local fsize = int(`fsize')	
				if (`fsize' > `maxfsize') {
				local maxfsize "`fsize'"
				}
			}
			local fsizes "`fsizes' `fsize'"

		}

		file read `fh' line
	
	}

	file close `fh'

	return local maxfsize `maxfsize'
	
end





program define checkdta

version 10
syntax [, startfolder(string)]

quietly: set linesize 255
quietly: cap log close
quietly: cap log using "../output/checkdta.log", replace text name(checkdtalog)
quietly: log off checkdtalog
set more off


if "`startfolder'" == "" {
	local startfolder "..\output"
}


quietly: log on checkdtalog
display "=========================================================="
display "Checkdta.ado Report:  
display "For every .dta file in ../output, checkdta.ado opens the file,"
display "runs datasignature and sum, and outputs to this log."
display ""
display "Start folder: `startfolder'"
display "=========================================================="
display ""
display ""
quietly: log off checkdtalog

maxmem
local maxfsize = "`r(maxfsize)'"
local maxfsize = subinstr("`maxfsize'",",","",.)
local maxfsize = int(`maxfsize')
local maxfsize = round(`maxfsize'*0.0009765625,0.1)
local maxfsize = int(1.2* `maxfsize')+200
clear
set mem `maxfsize'
local dtafiles: dir "`startfolder'" files "*.dta"
local dtafiles: list sort dtafiles
foreach dtafile of local dtafiles {
	local brandnewtotal =`"`startfolder'"'+ "\" + `"`dtafile'"'
	quietly: log on checkdtalog
	display ""
	display ""
	display ""
	display ""
	display "============================================="
	display "File: `brandnewtotal'"
	display "============================================="
	use "`brandnewtotal'", clear
	datasignature
	sum
	clear
	quietly: log off checkdtalog
}

local folders : dir "`startfolder'" dirs "*"
foreach folder of local folders{
	if strpos("`folder'", ".") ==0 {
		if "`folder'" != "" {
			local total = "`startfolder'" + "\"+ "`folder'"
			
			local dtafiles: dir "`total'" files "*.dta"
			local dtafiles: list sort dtafiles
			foreach dtafile of local dtafiles {
				local brandnewtotal =`"`total'"'+ "\" + `"`dtafile'"'
				quietly: log on checkdtalog
				display ""
				display ""
				display ""
				display ""
				display "============================================="
				display "File: `brandnewtotal'"
				display "============================================="

				use "`brandnewtotal'", clear
				datasignature
				sum
				
				quietly: log off checkdtalog

			}
			
			local subfolders : dir "`total'" dirs "*"		
			foreach subfolder of local subfolders {
				if strpos("`subfolder'", ".") ==0 {
					if "`subfolder'" != "" {
						local subtotal = "`total'" + "\" + "`subfolder'"
						
						local dtafiles: dir "`subtotal'" files "*.dta"
						local dtafiles: list sort dtafiles
						foreach dtafile of local dtafiles {
							local brandnewtotal =`"`subtotal'"'+ "\" + `"`dtafile'"'
							quietly: log on checkdtalog
							display ""
							display ""
							display ""
							display ""
							display "============================================="
							display "File: `brandnewtotal'"
							display "============================================="
							
							use "`brandnewtotal'", clear
							datasignature
							sum

							quietly: log off checkdtalog
						}
						
						
						local subsubfolders : dir "`subtotal'" dirs "*"
						foreach subsubfolder of local subsubfolders {
							if strpos("`subsubfolder'", ".") ==0 {
								if "`subsubfolder'" != "" {
									local subsubtotal =`"`subtotal'"'+"\"+ `"`subsubfolder'"'
									
									local dtafiles: dir "`subsubtotal'" files "*.dta"
									local dtafiles: list sort dtafiles
									foreach dtafile of local dtafiles {
										local brandnewtotal =`"`subsubtotal'"'+ "\" + `"`dtafile'"'
										quietly: log on checkdtalog
										display ""
										display ""
										display ""
										display ""
										display "============================================="
										display "File: `brandnewtotal'"
										display "============================================="
										
										use "`brandnewtotal'", clear
										datasignature
										sum
										
										quietly: log off checkdtalog
							
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
quietly: log close checkdtalog
end

checkdta
