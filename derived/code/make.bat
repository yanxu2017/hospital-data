REM ****************************************************
REM * make.bat: double-click to run all scripts
REM ****************************************************

REM ******************************************************
REM * Manually copy-paste the raw data files into external
REM ******************************************************

SET LOG=..\output\make.log

REM DELETE OUTPUT & TEMP FILES
DEL /F /Q ..\output\
RMDIR ..\temp /S /Q
mkdir ..\temp
RMDIR ..\output /S /Q
mkdir ..\output


REM LOG START
ECHO make.bat started	>%LOG%
ECHO %DATE%		>>%LOG%
ECHO %TIME%		>>%LOG%
dir ..\output\ >>%LOG%

%STATAEXE% /e do preclean_outpatient.do
%STATAEXE% /e do checkdta.do

COPY %LOG%+preclean_outpatient.log+checkdta.do %LOG%
 
DEL preclean_outpatient.log checkdta.do

REM CLOSE LOG
ECHO make.bat completed	>>%LOG%
ECHO %DATE%		>>%LOG%
ECHO %TIME%		>>%LOG%

PAUSE
